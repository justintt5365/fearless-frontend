function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
        <div class="col col-sm-6 col-lg-4 mb-4">
            <div class="card shadow-lg mb-5 bg-body rounded">
                <img src="${pictureUrl}" class="bd-placeholder-img card-img-top" width="100%" height="200" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Image cap" preserveAspectRatio="xMidYMid slice" focusable="false">
                <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <span style="color:grey;">${location}</span>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
                <p>${startDate} - ${endDate}</p>
            </div>
        </div>
    `;
}

const alertPlaceholder = document.getElementById('liveAlertPlaceholder')

function alert(message, type) {
  const wrapper = document.createElement('div')
  wrapper.innerHTML = '<div class="alert alert-' + type + ' alert-dismissible" role="alert">' + message + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'

  alertPlaceholder.append(wrapper)
  
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        alert("you triggered an alert", 'danger');
        

      } else {
        const data = await response.json();
  
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const starts = details.conference.starts;
            const ends = details.conference.ends;
            const startDate = starts.slice(0, -15);
            const endDate = ends.slice(0, -15);
            const location = details.conference.location.name
            const html = createCard(name, description, pictureUrl, startDate, endDate, location);
            const column = document.querySelector('.row');
            column.innerHTML += html;
          }
        }
  
      }
    } catch (error) {
        console.error(error)
        alert("you triggered an alert", 'danger');
      // Figure out what to do if an error is raised
    }
  
  });