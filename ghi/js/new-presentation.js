window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json(); 
            let selectTag = document.getElementById('conference');
            
            for (let conference of data.conferences){
                console.log(conference);
                let option = document.createElement('option');
                option.value = conference.pk;
                option.innerHTML = conference.name;
                selectTag.appendChild(option);
            }





        }
    } catch(error) {
        console.log(error);
    }


    const formTag = document.getElementById('create-presentation-form');
    formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            const confPk = ((Object.fromEntries(formData)).conference)
            const presentationsUrl = `http://localhost:8000/api/conferences/${confPk}/presentations/`;
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(presentationsUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newConference = await response.json();
            }
    });
});