// Get the cookie out of the cookie store
const payloadCookie = await cookieStore.get('jwt_access_payload')
if (payloadCookie) {
  // The cookie value is a JSON-formatted string, so parse it
    const encodedPayload = JSON.parse(payloadCookie.value);
  // Convert the encoded payload from base64 to normal string
    const decodedPayload = atob(encodedPayload)
  // The payload is a JSON-formatted string, so parse it
    const payload = JSON.parse(decodedPayload)

//   Print the payload
    console.log(payload);

  // Check if "events.add_conference" is in the permissions.
    if (payload.user.perms.includes('events.add_location')) {
        let selector = document.getElementById('new-location');
        selector.classList.remove('d-none')
    }
  // If it is, remove 'd-none' from the link


  // Check if "events.add_location" is in the permissions.
    if (payload.user.perms.includes('events.add_conference')) {
        let selector = document.getElementById('new-conference');
        selector.classList.remove('d-none')
    }
  // If it is, remove 'd-none' from the link

} else {
    console.log("cookie not found")
}