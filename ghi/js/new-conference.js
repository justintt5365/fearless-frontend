window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations/';

    try {
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json(); 
            let selectTag = document.getElementById('location');
            
            for (let location of data.locations){
                console.log(location);
                let option = document.createElement('option');
                option.value = location.pk;
                option.innerHTML = location.name;
                selectTag.appendChild(option);
            }





        }
    } catch(error) {
        console.log(error);
    }


    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            console.log(Object.fromEntries(formData))
            const conferencesUrl = 'http://localhost:8000/api/conferences/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(conferencesUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newConference = await response.json();
            }
    });
});