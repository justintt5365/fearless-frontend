import React from 'react';

class PresentationForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            presenterName: '',
            companyName: '',
            presenterEmail: '',
            title: '',
            synopsis: '',
            conferences: []
          };
          this.handlePresenterNameChange = this.handlePresenterNameChange.bind(this);
          this.handleCompanyNameChange = this.handleCompanyNameChange.bind(this);
          this.handlePresenterEmailChange = this.handlePresenterEmailChange.bind(this);
          this.handleTitleChange = this.handleTitleChange.bind(this);
          this.handleSynopsis = this.handleSynopsis.bind(this);
          this.handleConferenceChange = this.handleConferenceChange.bind(this);
          this.handleSubmit = this.handleSubmit.bind(this);
    }

    handlePresenterNameChange(event) {
        const value = event.target.value;
        this.setState({presenterName: value})
        }

    handleCompanyNameChange(event) {
        const value = event.target.value;
        this.setState({companyName: value})
        }

    handlePresenterEmailChange(event) {
        const value = event.target.value;
        this.setState({presenterEmail: value})
        }

    handleTitleChange(event) {
        const value = event.target.value;
        this.setState({title: value})
        }

    handleSynopsis(event) {
        const value = event.target.value;
        this.setState({synopsis: value})
        }

    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({location: value})
        }

    handleConferenceChange(event) {
        const value = event.target.value;
        this.setState({conference: value})
    }

    async handleSubmit(event) {
        event.preventDefault();
        console.log({...this.state})
        const data = {...this.state};
        data.presenter_name = data.presenterName;
        delete data.presenterName;
        data.company_name = data.companyName;
        delete data.companyName;
        data.presenter_email = data.presenterEmail;
        delete data.presenterEmail;
        delete data.conferences;
        console.log(data);
        
        const confHref = data.conference
        const presentationUrl = `http://localhost:8000${confHref}presentations/`
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);
            const cleared = {
            presenterName: '',
            companyName: '',
            presenterEmail: '',
            title: '',
            synopsis: '',
            conference: '',
            };
            this.setState(cleared);
        }
    }

    async componentDidMount() {
        const url = 'http://localhost:8000/api/conferences/';
    
        const response = await fetch(url);
        // console.log(response)
    
        if (response.ok) {
          const data = await response.json();
          this.setState({conferences: data.conferences});
          
        }
      }

    render() {
      return (
<div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={this.handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={this.handlePresenterNameChange} value={this.state.presenterName} placeholder="presenter_name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                <label htmlFor="name">Presenter Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handlePresenterEmailChange} value={this.state.presenterEmail} placeholder="presenter_email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                <label htmlFor="starts">Presenter Email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleCompanyNameChange} value={this.state.companyName} placeholder="company_name" required type="text" name="company_name" id="company_name" className="form-control"/>
                <label htmlFor="ends">Company Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleTitleChange} value={this.state.title} placeholder="title" required type="text" name="title" id="title" className="form-control"/>
                <label htmlFor="ends">Title</label>
              </div>
                <div className="mb-3">
                  <label htmlFor="Synopsis" className="form-label">Synopsis</label>
                  <textarea onChange={this.handleSynopsis} value={this.state.synopsis} className="form-control" id="Synopsis" name="Synopsis" rows="3"></textarea>
                </div>
              <div className="mb-3">
                <select onChange={this.handleConferenceChange} value={this.state.conference} required name="conference" id="conference" className="form-select">
                  <option value="">Choose a conference</option>
                  {this.state.conferences.map(conference => {
                    return (
                    <option key={conference.href} value={conference.href}>
                        {conference.name}
                    </option>
                    );
                })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
      );
    }
  }

export default PresentationForm;